from configuracion import *
from principal import *
import math
import random
import pygame
from pygame.locals import *

#Esta funcion compara las coincidencias entre las elecciones del usuario y las de la pc y asignar un puntaje si hay conicidencia
def ComparacionUsuarioPC(eleccionUsuario,eleccionCompu):
    puntajeCoincidencias=0
    for palabraUsuario in eleccionUsuario: # recorre la lista de eleccion de usuario
        for palabraCompu in eleccionCompu:# este for me sirve para comparar cada eleccion de usuario con todas las elecciones de la pc
            if palabraCompu==palabraUsuario:
                puntajeCoincidencias+=50
    return puntajeCoincidencias

# funcion auxiliar de juegaCompu. Me agrega los elementos a la lista de salida=[]
def AgregarElementosListFinal(contador,listaa,salida):
    if  contador>=1: # si el contador de repeticiones es mayor o igual a 1, me elige uno de los elementos de la lista y me lo agrega a la lista de salida
        elementoParaAgregar=random.randint(0,contador-1)
        salida.append(listaa[elementoParaAgregar])
    else:
        salida.append("") # en caso de ser 0 osea que no hay elementos en la lista1 o listaa, me agrega a la lista final ""

    return salida


def unaAlAzar(lista):
    cantidadElementosLista = len(lista) # me guarda en una variable la cantidad de elementos de la lista que le pase (abecedario)
    indice = random.randint(0,cantidadElementosLista-1) # teniendo en cuenta la cantidad de elementos de la lista me elije un numero al azar que luego ser? utilizado para ?ndice y retornarlo
    return lista[indice]


def esCorrecta(palabraUsuario, letra, item, items, listaDeTodo):
    pygame.init() # me trae los sonidos de win y lost al programa
    sonidoLost=pygame.mixer.Sound("sounds/homero13.wav")
    sonidoWin=pygame.mixer.Sound("sounds/ay-caramba.wav")
    a=1
    x=0 # bandera que me va a indicar luego si hubo una coicidencia
    indiceListaMayor=0
    for caracter in palabraUsuario: # me recorre los caracteres de la palabra del usuario
        if a == 1:
            if caracter == letra:
                for elemento in items: # si es el primer caracter y este es igual a la letra al azar, recorro los elemento de la lista item
                    if elemento==item: # si el elemento es igual al item[i] sobre el cual se trabaja, luego recorro los elementos que se encuentran dentro de la lista de categorias se?alizando la lista en la que debo buscar con el IndicelistaMayor, si no es igual le sumo uno a este indice y vuelvo a probar
                        for elementin in listaDeTodo[indiceListaMayor]:
                            if elementin==palabraUsuario: # ya ubicado dentro de la categoria en donde tengo que buscar, me fijo si hay alguno que coincida con la palabra del usuario
                                x=1
                                sonidoWin.play() # si hay una coicidencia me reproduce el sonido win y me suma 10 puntos
                                return 10


                        if x==0: # si la bandera de coicidencias se mantuvo en 0 me reprouce el sonido lost y me resta 10 puntos
                            sonidoLost.play()
                            return -10

                    else:
                        indiceListaMayor+=1
            else: # si el primer caracter no es igual a la la letra al azar me resta 10 puntos y me reproduce el sonido lost
                sonidoLost.play()
                return -10
    a+=1

def juegaCompu(letraAzar, listaDeTodo):
    pygame.init()
    sonidoEnd=pygame.mixer.Sound("sounds/monoriel.wav") # trae el sonido al programa
    salida=[]
    for elemento in listaDeTodo: # recorre la lista que contiene las categorias
        lista1=[]
        contadorRepeticiones=0
        for elementito in elemento: # recorre cada lista que se encuentra dentro de la lista que contiene todas
            a=0
            for caracter in elementito:# recorre cada caracter de los elementos que componen cada lista de categorias
                if a==0: # con este condicional le digo al programa que si esta en el primer caracter, siga por esta rama
                    if caracter==letraAzar: # si ese primer caracter es igual a la letra al azar que me lo agrege a una lista y me sume una repeticion
                        lista1.append(elementito)
                        contadorRepeticiones+=1
                a+=1

        salida = AgregarElementosListFinal(contadorRepeticiones,lista1,salida)
    sonidoEnd.play() # ejecuta el sonido traido al programa
    return salida





